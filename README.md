![Python Banner](https://raspberry-valley.azurewebsites.net/img/Python-01.jpg)

### Resumen del Código

Este script en Python automatiza la generación de registros de horarios laborales simulados y los inserta en una base de datos PostgreSQL local. Utiliza la librería `psycopg2` para la conexión y manipulación de la base de datos, y genera registros de entrada y salida para varios usuarios a lo largo de los últimos dos meses.

### Configuración del Entorno Virtual (venv) en Python

#### Creación del Entorno Virtual en Windows 10

1. **Abrir la Terminal**:
   - Busca "cmd" o "PowerShell" en el menú de inicio y ábrelo como administrador.

2. **Navegar a la Carpeta del Proyecto**:
   ```bash
   cd ruta/al/proyecto
   ```

3. **Crear el Entorno Virtual**:
   ```bash
   python -m venv venv
   ```

4. **Activar el Entorno Virtual**:
   - En PowerShell:
     ```bash
     .\venv\Scripts\Activate.ps1
     ```
   - En cmd:
     ```bash
     .\venv\Scripts\activate
     ```

#### Activación del Entorno Virtual en Ubuntu

1. **Abrir la Terminal**:
   - Usa `Ctrl + Alt + T` para abrir la terminal.

2. **Navegar a la Carpeta del Proyecto**:
   ```bash
   $ cd ruta/al/proyecto
   ```

3. **Crear el Entorno Virtual** (si no está creado):
   ```bash
   $ python3 -m venv venv
   ```

4. **Activar el Entorno Virtual**:
   ```bash
   $ source venv/bin/activate
   ```

### Uso del `requirements.txt`

Una vez que el entorno virtual está activado, puedes instalar las dependencias necesarias utilizando el archivo `requirements.txt`.

```bash
$ pip install -r requirements.txt
```

### Ejecución del Script

Asegúrate de tener tu servidor PostgreSQL local en ejecución y configurado correctamente con una base de datos llamada "schedules", accesible con el usuario "root" y la contraseña "root". Luego, puedes ejecutar el script Python `main.py`.

```bash
$ python3 main.py
```

Este script generará registros simulados de horarios laborales en la base de datos especificada, utilizando datos aleatorios para la hora de entrada y salida de varios usuarios durante los últimos dos meses.

### Nota

- Asegúrate de tener instalado Python y PostgreSQL correctamente configurado en tu sistema antes de ejecutar el script.
- Modifica los detalles de conexión (`dbname`, `user`, `password`, `host`, `port`) en el script según tu configuración de base de datos PostgreSQL local.