import psycopg2
from datetime import datetime, timedelta
import random

# Conexión a la base de datos 'users'
conn_users = psycopg2.connect(
    dbname="users",
    user="root",
    password="root",
    host="localhost",
    port="5432"
)
cur_users = conn_users.cursor()

# Obtener los IDs de usuarios desde la base de datos 'users'
cur_users.execute("SELECT id FROM users")
user_ids = [row[0] for row in cur_users.fetchall()]

# Cerrar la conexión con 'users'
cur_users.close()
conn_users.close()

# Conexión a la base de datos 'schedules'
conn_schedules = psycopg2.connect(
    dbname="schedules",
    user="root",
    password="root",
    host="localhost",
    port="5432"
)
cur = conn_schedules.cursor()

# Configuración de fechas
end_date = datetime.now()
start_date = end_date - timedelta(days=60)  # 2 meses atrás

# Generar fechas de lunes a viernes
def generate_workdays(start_date, end_date):
    current_date = start_date
    while current_date <= end_date:
        if current_date.weekday() < 5:  # Lunes a viernes
            yield current_date
        current_date += timedelta(days=1)

# Insertar registros en la base de datos 'schedules'
for user_id in user_ids:
    for workday in generate_workdays(start_date, end_date):
        # Generar hora de entrada entre las 7 am y 8 am
        entry_time = workday.replace(hour=7, minute=random.randint(0, 59))
        entry_time = entry_time + timedelta(minutes=random.randint(0, 59))

        # Generar hora de salida entre las 5 pm y 6 pm
        exit_time = workday.replace(hour=17, minute=random.randint(0, 59))
        exit_time = exit_time + timedelta(minutes=random.randint(0, 59))

        # Insertar en la tabla income
        cur.execute("""
            INSERT INTO income (date, latitude, longitude, altitude)
            VALUES (%s, %s, %s, %s) RETURNING income_id
        """, (entry_time, -29.9380976 + (random.random() * 0.01), -71.190297 + (random.random() * 0.01), random.uniform(0, 300)))
        income_id = cur.fetchone()[0]

        # Insertar en la tabla outcome
        cur.execute("""
            INSERT INTO outcome (date, latitude, longitude, altitude)
            VALUES (%s, %s, %s, %s) RETURNING outcome_id
        """, (exit_time, -29.9380976 + (random.random() * 0.01), -71.190297 + (random.random() * 0.01), random.uniform(0, 300)))
        outcome_id = cur.fetchone()[0]

        # Insertar en la tabla schedules
        cur.execute("""
            INSERT INTO schedules (user_id, income_made_by, outcome_made_by, income_id, outcome_id)
            VALUES (%s, %s, %s, %s, %s)
        """, (user_id, 'worker', 'worker', income_id, outcome_id))

# Confirmar los cambios en 'schedules'
conn_schedules.commit()

# Cerrar la conexión con 'schedules'
cur.close()
conn_schedules.close()
